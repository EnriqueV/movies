import 'dart:convert';
import 'package:http/http.dart' as http;

class Api {
  static final Map<String, String> _headers = {
    "Access-Control-Allow-Origin": "*",
    "Content-Type": "application/json",
  };

  static Future<http.Response> findAll() => http.get(
        Uri.parse(
            "https://api.themoviedb.org/3/movie/popular?api_key=58bdc144214626e3737aef22a1267954"),
        headers: Api._headers,
      );
}
