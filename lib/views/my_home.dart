import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:movies/views/dashboard.dart';
import 'login.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  void initState() {
    // set time to load
    _startApp();
    super.initState();
  }

  Future<void> _startApp() async {
    final storage = FlutterSecureStorage();
    String? token = await storage.read(key: "token");
    if (token == null) {
      Future.delayed(Duration(seconds: 5), () {
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => Login()));
      });
    } else {
      Future.delayed(Duration(seconds: 5), () {
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => DashBoard()));
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          alignment: Alignment.center,
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  height: 300,
                  width: 400,
                  child: Lottie.asset('assets/lottie/splash_movie.json'),
                ),
                SizedBox(
                  height: 20,
                ),
              ])),
    );
  }
}
