import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import '../models/movie.dart';

class MovieDetails extends StatefulWidget {
  final Movie movie;
  MovieDetails({Key? key, required this.movie}) : super(key: key);

  @override
  State<MovieDetails> createState() => _MovieDetailsState();
}

class _MovieDetailsState extends State<MovieDetails> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            appBar: AppBar(
              title: const Text('Movie Details'),
              automaticallyImplyLeading: false,
              centerTitle: true,
              leading: IconButton(
                icon: Icon(Icons.arrow_back, color: Colors.black),
                onPressed: () => Navigator.of(context).pop(),
              ),
            ),
            body: Card(
              elevation: 5,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(14),
              ),
              child: InkWell(
                // onTap: () => Navigator.push(
                //     context,
                //     MaterialPageRoute(
                //         builder: (context) => MovieDetails(movie: _data))),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(14),
                  child: Stack(
                    children: [
                      Image.network(
                        "https://image.tmdb.org/t/p/original${widget.movie.posterPath}",
                        height: double.maxFinite,
                        fit: BoxFit.cover,
                        width: double.maxFinite,
                      ),
                      Container(
                        height: double.maxFinite,
                        width: double.maxFinite,
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            colors: [
                              Colors.blueAccent,
                              Colors.transparent,
                            ],
                            begin: FractionalOffset(0.0, 1.0),
                            end: FractionalOffset(0.0, 0.0),
                            stops: [0.0, 0.8],
                            tileMode: TileMode.clamp,
                          ),
                        ),
                        child: Column(
                          children: [
                            Expanded(child: Container()),
                            Text(
                              widget.movie.title ?? '',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 22,
                                height: 1.3,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            Text(
                              '${widget.movie.overview}',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 22,
                                height: 1.3,
                                fontWeight: FontWeight.normal,
                              ),
                            ),
                            Text(
                              'Release: ${widget.movie.releaseDate}',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 22,
                                height: 1.3,
                                fontWeight: FontWeight.normal,
                              ),
                            ),
                            Text(
                              'Average: ${widget.movie.voteAverage}',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 22,
                                height: 1.3,
                                fontWeight: FontWeight.normal,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            )));
  }
}
