import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:movies/utils/api.dart';
import 'package:http/http.dart' as http;
import '../models/movie.dart';
import 'movie_details.dart';
import 'search.dart';

class DashBoard extends StatefulWidget {
  DashBoard({Key? key}) : super(key: key);

  @override
  State<DashBoard> createState() => _DashBoardState();
}

class _DashBoardState extends State<DashBoard> {
  List<Movie> _movies = [];
  TextEditingController searchQuery = TextEditingController();

  void textSubmit() {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => Search(
          searchQuery: searchQuery.text,
        ),
      ),
    );
  }

  Future fetchMovies() async {
    final response = await http.get(Uri.parse(
        'https://api.themoviedb.org/3/movie/popular?api_key=58bdc144214626e3737aef22a1267954'));

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      Iterable _iterable = jsonDecode(response.body)['results'];

      setState(
        () =>
            _movies = _iterable.map((movie) => Movie.fromJson(movie)).toList(),
      );
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load movies');
    }
  }

  @override
  void initState() {
    super.initState();
    fetchMovies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Popular Movies'),
        automaticallyImplyLeading: false,
        centerTitle: true,
      ),
      body: Column(
        children: [
          const SizedBox(
            height: 20,
          ),
          Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 30).copyWith(bottom: 10),
            child: TextField(
              controller: searchQuery,
              style: const TextStyle(color: Colors.black, fontSize: 14.5),
              decoration: InputDecoration(
                  filled: true,
                  fillColor: Colors.grey,
                  prefixIconConstraints: const BoxConstraints(minWidth: 45),
                  suffixIconConstraints:
                      const BoxConstraints(minWidth: 45, maxWidth: 46),
                  border: InputBorder.none,
                  hintText: 'Search Movie',
                  hintStyle:
                      const TextStyle(color: Colors.black, fontSize: 14.5),
                  enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(100)
                          .copyWith(bottomRight: const Radius.circular(0)),
                      borderSide: const BorderSide(color: Colors.white38)),
                  focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(100)
                          .copyWith(bottomRight: const Radius.circular(0)),
                      borderSide: const BorderSide(color: Colors.white70))),
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          GestureDetector(
            onTap: () {
              textSubmit();
            },
            child: Container(
              height: 53,
              width: double.infinity,
              margin: const EdgeInsets.symmetric(horizontal: 30, vertical: 20),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                      blurRadius: 4,
                      color: Colors.black12.withOpacity(.2),
                      offset: const Offset(2, 2))
                ],
                borderRadius: BorderRadius.circular(100)
                    .copyWith(bottomRight: const Radius.circular(0)),
                color: Colors.blue.shade300,
              ),
              child: Text('Search',
                  style: TextStyle(
                      color: Colors.white.withOpacity(.8),
                      fontSize: 15,
                      fontWeight: FontWeight.bold)),
            ),
          ),
          Expanded(
            child: GridView.count(
              physics: BouncingScrollPhysics(),
              padding: EdgeInsets.symmetric(horizontal: 20),
              crossAxisCount: 2,
              mainAxisSpacing: 15,
              crossAxisSpacing: 15,
              shrinkWrap: true,
              childAspectRatio: 1,
              children: _movies.map((e) => CategoryItem(data: e)).toList(),
            ),
          ),
        ],
      ),
    );
  }
}

class CategoryItem extends StatelessWidget {
  const CategoryItem({
    Key? key,
    required data,
  })  : _data = data,
        super(key: key);

  final Movie _data;

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(14),
      ),
      child: InkWell(
        onTap: () => Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => MovieDetails(movie: _data))),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(14),
          child: Stack(
            children: [
              Image.network(
                "https://image.tmdb.org/t/p/original${_data.posterPath}",
                height: double.maxFinite,
                fit: BoxFit.cover,
                width: double.maxFinite,
              ),
              Container(
                height: double.maxFinite,
                width: double.maxFinite,
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      Colors.blueAccent,
                      Colors.transparent,
                    ],
                    begin: FractionalOffset(0.0, 1.0),
                    end: FractionalOffset(0.0, 0.0),
                    stops: [0.0, 0.8],
                    tileMode: TileMode.clamp,
                  ),
                ),
                child: Column(
                  children: [
                    Expanded(child: Container()),
                    Text(
                      _data.title ?? '',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 22,
                        height: 1.3,
                        fontWeight: FontWeight.bold,
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class CategorySelectorItem extends StatelessWidget {
  const CategorySelectorItem({
    Key? key,
    required String cats,
    bool? active,
    void Function(String)? onTap,
  })  : _name = cats,
        _active = active ?? false,
        _onTap = onTap,
        super(key: key);

  final String _name;
  final bool _active;
  final void Function(String)? _onTap;

  @override
  Widget build(BuildContext context) {
    return CupertinoButton(
      child: Text(
        _name,
        style: TextStyle(
          color: _active ? Colors.black : Colors.grey,
          fontWeight: FontWeight.w600,
          fontSize: 20,
          decoration: _active ? TextDecoration.underline : TextDecoration.none,
        ),
      ),
      onPressed: () {
        _onTap?.call(_name);
      },
    );
  }
}
