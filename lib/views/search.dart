import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:lottie/lottie.dart';

import '../models/movie.dart';
import 'movie_details.dart';

class Search extends StatefulWidget {
  Search({Key? key, required this.searchQuery}) : super(key: key);

  final String searchQuery;

  @override
  State<Search> createState() => _SearchState();
}

class _SearchState extends State<Search> {
  List<Movie> _movies = [];
  bool loading = true;
  @override
  void initState() {
    // set time to load
    fetchMovies();
    super.initState();
  }

  Future fetchMovies() async {
    final response = await http.get(Uri.parse(
        'https://api.themoviedb.org/3/search/movie?api_key=58bdc144214626e3737aef22a1267954&query=${widget.searchQuery}'));

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      Iterable _iterable = jsonDecode(response.body)['results'];

      setState(
        () =>
            _movies = _iterable.map((movie) => Movie.fromJson(movie)).toList(),
      );

      setState(() {
        loading = false;
      });
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load movies');
    }
  }

  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Searching Movies'),
          automaticallyImplyLeading: false,
          centerTitle: true,
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.black),
            onPressed: () => Navigator.of(context).pop(),
          ),
        ),
        body: Column(
          children: [
            loading
                ? Loading()
                : Expanded(
                    child: GridView.count(
                      physics: BouncingScrollPhysics(),
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      crossAxisCount: 2,
                      mainAxisSpacing: 15,
                      crossAxisSpacing: 15,
                      shrinkWrap: true,
                      childAspectRatio: 1,
                      children:
                          _movies.map((e) => CategoryItem(data: e)).toList(),
                    ),
                  ),
          ],
        ));
  }
}

class Loading extends StatelessWidget {
  const Loading({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        alignment: Alignment.center,
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                height: 300,
                width: 400,
                child: Lottie.asset('assets/lottie/movie_loading.json'),
              ),
              SizedBox(height: 20),
            ]));
  }
}

class CategoryItem extends StatelessWidget {
  const CategoryItem({
    Key? key,
    required data,
  })  : _data = data,
        super(key: key);

  final Movie _data;

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(14),
      ),
      child: InkWell(
        onTap: () => Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => MovieDetails(movie: _data))),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(14),
          child: Stack(
            children: [
              Image.network(
                "https://image.tmdb.org/t/p/original${_data.posterPath}",
                height: double.maxFinite,
                fit: BoxFit.cover,
                width: double.maxFinite,
              ),
              Container(
                height: double.maxFinite,
                width: double.maxFinite,
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      Colors.blueAccent,
                      Colors.transparent,
                    ],
                    begin: FractionalOffset(0.0, 1.0),
                    end: FractionalOffset(0.0, 0.0),
                    stops: [0.0, 0.8],
                    tileMode: TileMode.clamp,
                  ),
                ),
                child: Column(
                  children: [
                    Expanded(child: Container()),
                    Text(
                      _data.title ?? '',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 22,
                        height: 1.3,
                        fontWeight: FontWeight.bold,
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class CategorySelectorItem extends StatelessWidget {
  const CategorySelectorItem({
    Key? key,
    required String cats,
    bool? active,
    void Function(String)? onTap,
  })  : _name = cats,
        _active = active ?? false,
        _onTap = onTap,
        super(key: key);

  final String _name;
  final bool _active;
  final void Function(String)? _onTap;

  @override
  Widget build(BuildContext context) {
    return CupertinoButton(
      child: Text(
        _name,
        style: TextStyle(
          color: _active ? Colors.black : Colors.grey,
          fontWeight: FontWeight.w600,
          fontSize: 20,
          decoration: _active ? TextDecoration.underline : TextDecoration.none,
        ),
      ),
      onPressed: () {
        _onTap?.call(_name);
      },
    );
  }
}
